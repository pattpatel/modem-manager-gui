modem-manager-gui (0.0.20-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Update watch file format version to 4.
  * Update standards version to 4.6.0, no changes needed.
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on ofono-dev.
    + modem-manager-gui: Drop versioned constraint on connman, modemmanager,
      network-manager, ofono and ppp in Depends.

  [ Graham Inggs ]
  * Fix build with meson 0.61 (Closes: #1005596)

 -- Graham Inggs <ginggs@debian.org>  Sun, 27 Mar 2022 19:57:47 +0000

modem-manager-gui (0.0.20-2) unstable; urgency=medium

  * Fix tray icon (Closes: #969231)

 -- Graham Inggs <ginggs@debian.org>  Thu, 03 Sep 2020 13:14:06 +0000

modem-manager-gui (0.0.20-1) unstable; urgency=medium

  * New upstream release
  * Drop patches included upstream
  * Mark modem-manager-gui-help Multi-Arch: foreign
  * Drop Breaks and Replaces on ancient versions

 -- Graham Inggs <ginggs@debian.org>  Sun, 16 Aug 2020 14:06:55 +0000

modem-manager-gui (0.0.19.1-4) unstable; urgency=medium

  * Fix help pages format errors
  * Switch to Meson build system (Closes: #960938)
  * Switch to debhelper 13
  * Do not link with -Wl,--as-needed, it is now the default

 -- Graham Inggs <ginggs@debian.org>  Sun, 26 Jul 2020 18:41:44 +0000

modem-manager-gui (0.0.19.1-3) unstable; urgency=medium

  [ Graham Inggs ]
  * Switch to debhelper 12
  * Set Rules-Requires-Root: no
  * Switch from libappindicator to libayatana-appindicator (Closes: #956775)
  * Bump Standards-Version to 4.5.0, no changes

  [ Debian Janitor ]
  * Fix field name typos in debian/copyright.

 -- Graham Inggs <ginggs@debian.org>  Sat, 25 Apr 2020 13:20:23 +0000

modem-manager-gui (0.0.19.1-2) unstable; urgency=medium

  * Fix memory corruption because of wrong strsep() usage (Closes: #922608)
  * Fix segfault in strftime_l() because of timestamps from future
  * Use secure URI in debian/watch
  * Bump Standards-Version to 4.3.0, no changes

 -- Graham Inggs <ginggs@debian.org>  Sat, 25 May 2019 12:28:34 +0000

modem-manager-gui (0.0.19.1-1) unstable; urgency=medium

  * New upstream release
    - Fix linker errors (Closes: #893901)
    - Expand SMS folders on first messages
    - Fix spelling errors reported by Lintian
    - Fix polkit preventing module loading without systemd
    - Use generic term 'connection manager' instead of
      'network manager' in warning
    - Fix typo in Russian translation
    - Add Slovak localization

 -- Graham Inggs <ginggs@debian.org>  Sat, 07 Apr 2018 10:12:18 +0000

modem-manager-gui (0.0.19-1) unstable; urgency=medium

  * New upstream release
    - Deprecated GtkStatusIcon has been replaced with AppIndicator
      (Closes: #826399)
    - Stalled connection to Akonadi server bug has been fixed
      (Closes: #834697)
  * Update Vcs-* URIs for move to salsa.debian.org
  * Drop handle-ucs2-mccmnc.patch included upstream
  * Update debian/control, debian/clean and
    debian/modem-manager-gui.install for new upstream version
  * Turn debhelper up to 11
  * Update Homepage and debian/copyright
  * Bump Standards-Version to 4.1.3, no changes

 -- Graham Inggs <ginggs@debian.org>  Fri, 23 Mar 2018 14:17:59 +0000

modem-manager-gui (0.0.18-4) unstable; urgency=medium

  * Switch to debhelper 10
  * Drop override_dh_strip; ddeb migration is complete
  * Drop -pie from DEB_BUILD_MAINT_OPTIONS; no longer needed (Closes: #865609)
  * Drop wader-core as an alternative dependency
  * Update debian/copyright
  * Bump Standards-Version to 4.1.0; no further changes

 -- Graham Inggs <ginggs@debian.org>  Mon, 11 Sep 2017 16:21:07 +0000

modem-manager-gui (0.0.18-3) unstable; urgency=medium

  * Migrate from modem-manager-gui-dbg to ddebs,
    bump debhelper build-dependency.
  * Use secure URIs for VCS fields.
  * Bump Standards-Version to 3.9.7, no changes needed.

 -- Graham Inggs <ginggs@debian.org>  Mon, 04 Apr 2016 16:00:32 +0200

modem-manager-gui (0.0.18-2) unstable; urgency=medium

  * Handle UCS2-encoded MCC/MNC (Closes: #802429)
  * Split the help into a separate package, thanks Lintian

 -- Graham Inggs <ginggs@debian.org>  Mon, 07 Dec 2015 11:07:48 +0200

modem-manager-gui (0.0.18-1) unstable; urgency=medium

  * New upstream release, includes:
    - Fix crash on NULL strings (Closes: #764693)
    - Fix libebook 3.16 API break (Closes: #790494)
  * Add Build-Depends on libgtkspell3-3-dev and
    let modem-manager-gui Depends on policykit-1.
  * Drop patches:
    - fix-hyphen-used-as-minus-sign.patch, fixed upstream
    - missing-ofono-pkg-config.patch, fixed in ofono (cf. #763111)
  * Update my email address, copyright years.

 -- Graham Inggs <ginggs@debian.org>  Tue, 13 Oct 2015 16:38:46 +0200

modem-manager-gui (0.0.17.1-2) unstable; urgency=medium

  * Update d/copyright:
    - add license info for appdata/appdata.its
    - add license info for help/* (Closes: #765574)
    - add license info for resources/signal-*.png

 -- Graham Inggs <graham@nerve.org.za>  Thu, 16 Oct 2014 20:39:29 +0200

modem-manager-gui (0.0.17.1-1) unstable; urgency=medium

  [ Graham Inggs ]
  * New upstream release.
  * Update d/patches:
    - drop patches included upstream
    - update fix-hyphen-used-as-minus-sign.patch
    - new missing-ofono-pkg-config.patch
  * Update d/control:
    - add Build-Depends, Depends and Recommends for new functionality
    - new package descriptions to make Lintian happy
    - bump Standards-Version to 3.9.6 (no changes)
  * Update d/copyright:
    - update copyright years to 2014
    - use updated GPL-3.0+ template
    - fix typo: messages-*.png instead of message-*.png
  * Update Lintian spelling-error-in-binary override.

  [ Marius B. Kotsbak ]
  * Add debug symbols package (Closes: #756936).

 -- Graham Inggs <graham@nerve.org.za>  Thu, 16 Oct 2014 00:26:16 +0200

modem-manager-gui (0.0.16-3) unstable; urgency=medium

  * Change architecture to linux-any (Closes: #721782).
  * New d/patches/clang_FTBFS_Wreturn-type.patch:
    Non-void functions should return a value (Closes: #743741).
    (Fixes FTBFS with clang instead of gcc)
  * Bump Standards-Version to 3.9.5, no changes.
  * Update d/watch.

 -- Graham Inggs <graham@nerve.org.za>  Sun, 08 Jun 2014 11:41:49 +0200

modem-manager-gui (0.0.16-2) unstable; urgency=low

  * Skip dh_auto_clean if Makefile_h does not exist.
    (Fixes FTBFS with debhelper >= 9.20130720)

 -- Graham Inggs <graham@nerve.org.za>  Tue, 03 Sep 2013 15:15:54 +0200

modem-manager-gui (0.0.16-1) unstable; urgency=low

  * Initial release (Closes: #710256).

 -- Graham Inggs <graham@nerve.org.za>  Tue, 30 Jul 2013 12:51:59 +0200
